# The “Notes” Template

This small project demonstrates a PostgreSQL/Node.js/Saxon-JS application with a development
environment centered around Docker containers.

## TL;DR

### Getting started

1. Build the custom Node.js container: `docker build --no-cache -t nodejs-template docker/nodejs`
2. Start the application: `./gradlew start`. Starting the containers will print the container ids.
3. Wait for PostgreSQL to initialize. You can use `docker logs {ID} -f` to
   watch the container logs until PostgreSQL reports that it’s “ready to accept connections”.
   (Replace `{ID}` with the container ID of the `postgis_template` container.)
4. Initialize the database: `./gradlew db_setup`
5. Create the tables: `./gradlew db_tables`
6. Load the initial data set: `./gradlew db_load_data`
7. Create a user id for yourself: `gradle -Puserid=id -Pusername="Your Name" -Ppassword=pass createuser`

Now you can see [the home page](http://localhost:8181/).

### Uploading notes

Creating a “file upload” page turns out to be quite complicated on the Node.js end, so there’s a simple
Python script in the `tools/` directory that will upload files to the system.

`python tools/putart.py --credentials id:pass data/sample.xml`


