DROP DATABASE template;
DROP OWNED BY teditor;
DROP USER teditor;
DROP USER tuser;
DROP GROUP tgroup;

CREATE GROUP tgroup;
CREATE USER tuser  WITH NOCREATEDB IN GROUP tgroup
       ENCRYPTED PASSWORD 'sekrit';
CREATE USER teditor WITH CREATEDB IN GROUP tgroup
       ENCRYPTED PASSWORD 'alsosekrit';
CREATE DATABASE template WITH OWNER = teditor;

GRANT CONNECT ON DATABASE template TO tuser;

GRANT USAGE ON SCHEMA public TO tuser;
REVOKE CREATE ON SCHEMA public FROM tuser;

\c template

-- This demo application doesn't actually use postgis
-- CREATE EXTENSION postgis;
