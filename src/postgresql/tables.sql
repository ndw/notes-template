DROP TABLE IF EXISTS ARTICLES;
DROP TABLE IF EXISTS USERS;

CREATE TABLE ARTICLES(
  URI        TEXT                     PRIMARY KEY NOT NULL,
  AUTHORID   TEXT                     NOT NULL,
  PUBDATE    TIMESTAMP WITH TIME ZONE NOT NULL,
  UPDATED    TIMESTAMP WITH TIME ZONE NOT NULL,
  TITLE      TEXT                     NOT NULL,
  ABSTRACT   TEXT,
  KEYWORDS   TEXT[],
  XMLTEXT    XML                      NOT NULL,
  SEARCHTEXT TEXT,
  TOKENS     TSVECTOR
);

CREATE TABLE USERS(
  ID          TEXT                  NOT NULL,
  NAME        TEXT                  NOT NULL,
  PASSWORD    TEXT                  NOT NULL
);

GRANT SELECT ON TABLE articles TO tuser;
GRANT SELECT ON TABLE users TO tuser;
