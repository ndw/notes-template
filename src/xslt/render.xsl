<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:array="http://www.w3.org/2005/xpath-functions/array"
                xmlns:h="http://www.w3.org/1999/xhtml"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns="http://www.w3.org/1999/xhtml"
                exclude-result-prefixes="array h map xs"
                version="3.0">

<xsl:import href="common.xsl"/>

<xsl:output method="html" html-version="5" encoding="utf-8" indent="no"/>

<xsl:template match="/">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="h:head">
  <xsl:copy>
    <xsl:apply-templates select="@*,node()"/>
    <xsl:call-template name="static-head-elements"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="h:x-input">
  <xsl:if test="@name and map:contains($props, @name)">
    <input value="{map:get($props, @name)}">
      <xsl:sequence select="@*"/>
    </input>
  </xsl:if>
</xsl:template>

<xsl:template match="h:x-message">
  <xsl:if test="map:contains($props, 'message') and $props?message != ''">
    <div class="x-message">
      <p>
        <xsl:sequence select="$props?message"/>
      </p>
    </div>
  </xsl:if>
</xsl:template>

<xsl:template match="h:x-count">
  <xsl:if test="map:contains($props, 'count')">
    <span>
      <xsl:sequence select="$props?count"/>
    </span>
  </xsl:if>
</xsl:template>

<xsl:template match="h:x-notelist" expand-text="yes">
  <xsl:if test="map:contains($props, 'notes') and array:size($props?notes) gt 0">
    <ul>
      <xsl:for-each select="array:flatten($props?notes)">
        <li>
          <a href="{.?uri}">{.?title}</a>
          <xsl:text>, </xsl:text>
          <xsl:sequence select="format-dateTime(xs:dateTime(.?updated),
                                                '[D01] [MNn,*-3] [Y0001]')"/>
        </li>
      </xsl:for-each>
    </ul>
  </xsl:if>
</xsl:template>

<xsl:template match="element()">
  <xsl:copy>
    <xsl:apply-templates select="@*,node()"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="attribute()|text()|comment()|processing-instruction()">
  <xsl:copy/>
</xsl:template>

</xsl:stylesheet>
