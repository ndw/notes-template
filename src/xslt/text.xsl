<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:db="http://docbook.org/ns/docbook"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                exclude-result-prefixes="db xs"
                version="3.0">

<xsl:output method="text" encoding="utf-8"/>

<xsl:template match="/">
  <xsl:variable name="text" as="xs:string*">
    <xsl:apply-templates select="//text()"/>
  </xsl:variable>
  <xsl:sequence select="string-join($text, ' ')"/>
</xsl:template>

<xsl:template match="text()">
  <xsl:if test="normalize-space(.) != ''">
    <xsl:sequence select="normalize-space(.)"/>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
