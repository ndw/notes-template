<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:array="http://www.w3.org/2005/xpath-functions/array"
                xmlns:h="http://www.w3.org/1999/xhtml"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns="http://www.w3.org/1999/xhtml"
		exclude-result-prefixes="array h map xs"
                version="3.0">

<xsl:param name="proptext" as="xs:string" required="true"/>
<xsl:param name="props" select="parse-json($proptext)"/>

<xsl:template name="static-head-elements" as="element()*">
  <meta name="viewport"
        content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" type="text/css" href="/css/template.css"/>
</xsl:template>

</xsl:stylesheet>
