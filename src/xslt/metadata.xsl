<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:db="http://docbook.org/ns/docbook"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                exclude-result-prefixes="db xs"
                version="3.0">

<xsl:output method="text" encoding="utf-8"/>

<xsl:template match="/">
  <xsl:choose>
    <xsl:when test="/db:article">
      <xsl:variable name="props" as="map(*)">
        <xsl:apply-templates/>
      </xsl:variable>
      <xsl:sequence select="serialize($props, map{'method':'json'})"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:variable name="props"
                    select="map { 'error': 'Document is not a DocBook article.' }"/>
      <xsl:sequence select="serialize($props, map{'method':'json'})"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="db:article" as="map(*)">
  <xsl:map>
    <xsl:map-entry key="'title'"
                   select="string((db:info/db:title|db:title)[1])"/>
    <xsl:map-entry key="'keywords'"
                   select="array { db:info/db:keywordset/db:keyword ! string(.) }"/>
    <xsl:if test="db:info/db:abstract">
      <xsl:map-entry key="'abstract'"
                     select="normalize-space(string(db:info/db:abstract))"/>
    </xsl:if>
  </xsl:map>
</xsl:template>

</xsl:stylesheet>
