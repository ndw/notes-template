require('dotenv').config();

const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const flash = require('connect-flash');

const umanage = require('./umanage');
const homepage = require('./homepage');
const upload = require('./upload');
const article = require('./article');

const app = express();

app.disable('x-powered-by');

app.use(flash());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ "extended": false }));
app.use(bodyParser.json({"type":["application/json"]}));
app.use(bodyParser.text({"type":["application/xml"]}));

umanage.setupApp(app);

app.use(cors());

app.use(function timeLog (req, res, next) {
  console.log(`${req.method} ${req.originalUrl}`);
  next();
});

app.put(/\/note\/.+$/, (request, response, next) => {
  if (request.isAuthenticated()) {
    next();
  } else {
    console.log("Not logged in.");
    response.redirect(`/login`);
  }
}, upload);

app.get(/\/note\/.+$/, article);

app.get('/', homepage);

app.listen(process.env.PORT, () => {
  console.log("Server listening");
});
