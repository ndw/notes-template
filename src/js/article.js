"use strict";

const SaxonJS = require('saxon-js');
const {pool} = require('./config');
const render_xsl = require('./render.sef.json');

const article = (request, response) => {
  const uri = request.url;

  let sql = `SELECT xmltext FROM articles WHERE uri = '${uri}'`;
  pool.query(sql, (error, results) => {
    if (error) {
      console.log(sql);
      console.log(error);
      throw error;
    }

    switch (results.rows.length) {
    case 0:
      response.status(404).send("Not found");
      break;
    case 1:
      const options = { "stylesheetInternal": render_xsl,
                        "sourceText": results.rows[0].xmltext,
                        "stylesheetParams": {},
                        "destination": "serialized" };
      SaxonJS.transform(options, "async")
        .then(output => {
          response.status(200).send(output.principalResult);
        }).catch(error => {
          response.status(400).send(error);
        });
      break;
    default:
      response.status(400).send("Bad request");
      break;
    }
  });
};

module.exports = article;
