#!/usr/bin/env node

const bcrypt = require('bcryptjs');
const {pool} = require('../wconfig');

if (process.argv.length != 5) {
  console.log("Usage: node password.js userid username password");
} else {
  const userid = process.argv[2];
  const username = process.argv[3];
  const plaintext = process.argv[4];
  const passwdHash = bcrypt.hashSync(plaintext, 10);

  let sql = `SELECT COUNT(name) FROM users WHERE id = '${userid}';`;
  pool.query(sql, (error, results) => {
    if (error) {
      console.log(sql);
      console.log(error);
      return;
    }

    if (results.rows[0].count === "0") {
      console.log(`Creating ${userid} for ${username}`);
      sql = "INSERT INTO users(id,name,password) VALUES ";
      sql += `('${userid}','${username}','${passwdHash}');`;
    } else {
      console.log(`Updating ${userid} for ${username}`);
      sql = `UPDATE users SET name = '${username}', `;
      sql += `password = '${passwdHash}' `;
      sql += `WHERE id = '${userid}';`;
    }

    pool.query(sql, (error, results) => {
      if (error) {
        console.log(sql);
        console.log(error);
        return;
      }
      //console.log(`Password '${plaintext}' = '${passwdHash}'`);
    });
  });
}
