const {Pool} = require('pg');

const env = process.env;

const connectionString = `postgresql://${env.DB_WUSER}:${env.DB_WPASSWORD}@${env.DB_HOST}:${env.DB_PORT}/${env.DB_DATABASE}`;

const pool = new Pool({
  connectionString: connectionString,
  ssl: false
});

module.exports = {pool};
