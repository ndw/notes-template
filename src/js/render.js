"use strict";

const SaxonJS = require('saxon-js');
const {pool} = require('./config');
const render_xsl = require('./render.sef.json');

const render = (request, response, props) => {
  let sql = `SELECT xmltext FROM articles WHERE uri = '${props.uri}'`;
  pool.query(sql, (error, results) => {
    if (error) {
      console.log(sql);
      console.log(error);
      throw error;
    }

    if (results.rows.length != 1) {
      response.status(400).send("Bad request");
    } else {
      const params = {
        "proptext": JSON.stringify(props)
      };

      const options = { "stylesheetInternal": render_xsl,
                        "sourceText": results.rows[0].xmltext,
                        "stylesheetParams": params,
                        "destination": "serialized" };
      SaxonJS.transform(options, "async")
        .then(output => {
          response.status(200).send(output.principalResult);
        }).catch(error => {
          response.status(400).send(error);
        });
    }
  });
};

module.exports = render;
