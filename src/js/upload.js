"use strict";

const SaxonJS = require('saxon-js');
const {pool} = require('./wconfig');
const render = require('./render');
const docbook_xsl = require('./docbook.sef.json');
const metadata_xsl = require('./metadata.sef.json');
const text_xsl = require('./text.sef.json');

const upload = (request, response) => {
   if (request.method === "PUT") {
    putArticle(request, response);
  } else {
    response.status(400).send("Bad request");
  }
};

const putArticle = (request, response) => {
  let options = { "stylesheetInternal": metadata_xsl,
                  "sourceText": request.body,
                  "stylesheetParams": {},
                  "destination": "serialized" };
  SaxonJS.transform(options, "async")
    .then(output => {
      const props = JSON.parse(output.principalResult);
      props.uri = request.url;
      if ("error" in props) {
        response.status(400).send(props.error);
      } else {
        const options = { "stylesheetInternal": docbook_xsl,
                          "sourceText": request.body,
                          "stylesheetParams": {},
                          "destination": "serialized" };
        SaxonJS.transform(options, "async")
          .then(output => {
            props.xmltext = output.principalResult;
            publish(request, response, props);
          }).catch(error => {
            response.status(400).send(error);
          });
      }
    }).catch(error => {
      response.status(400).send(error);
    });
};

const publish = (request, response, props) => {
  let sql = `SELECT count(uri) FROM articles WHERE uri = '${props.uri}'`;
  pool.query(sql, (error, results) => {
    if (error) {
      console.log(sql);
      console.log(error);
      throw error;
    }

    if (results.rows[0].count === "0") {
      databaseInsert(request, response, props);
    } else {
      databaseUpdate(request, response, props);
    }
  });
};

const databaseInsert = (request, response, props) => {
  const dt = (new Date()).toISOString();

  // Hack, we assume the author is the only one who ever uploads an article
  const authorid = request.session.passport.user.id;

  let sql = [];
  sql.push("INSERT INTO articles");
  sql.push("(uri,authorid,pubdate,updated,title,abstract,keywords,xmltext)");
  sql.push("VALUES(");
  sql.push(`'${props.uri}','${authorid}','${dt}','${dt}',`);
  sql.push(`'${props.title.replace(/'/g, "''")}',`);

  if (props.abstract && props.abstract.trim() !== "") {
    sql.push(`'${props.abstract}',`);
  } else {
    sql.push('NULL,');
  }

  if (props.keywords.length !== 0) {
    sql.push(`'{${props.keywords.join(",")}}',`);
  } else {
    sql.push('NULL,');
  }

  sql.push(`XMLPARSE (DOCUMENT '${props.xmltext.replace(/'/g, "''")}'));`);

  pool.query(sql.join(" "), (error, results) => {
    if (error) {
      console.log(sql);
      console.log(error);
      throw error;
    }

    searchText(request, response, props);
  });
};

const databaseUpdate = (request, response, props) => {
  const dt = (new Date()).toISOString();

  let sql = [];
  sql.push("UPDATE articles");
  sql.push(`SET updated = '${dt}',`);
  sql.push(`title = '${props.title.replace(/'/g, "''")}',`);

  if (props.abstract && props.abstract.trim() !== "") {
    sql.push(`abstract = '${props.abstract}',`);
  } else {
    sql.push('abstract = NULL,');
  }

  if (props.keywords.length !== 0) {
    sql.push(`keywords = '{${props.keywords.join(",")}}',`);
  } else {
    sql.push('keywords = NULL,');
  }

  sql.push(`xmltext = XMLPARSE (DOCUMENT '${props.xmltext.replace(/'/g, "''")}')`);
  sql.push(`WHERE uri = '${props.uri}'`);

  pool.query(sql.join(" "), (error, results) => {
    if (error) {
      console.log(sql);
      console.log(error);
      throw error;
    }

    searchText(request, response, props);
  });
};

const searchText = (request, response, props) => {
  let options = { "stylesheetInternal": text_xsl,
                  "sourceText": props.xmltext,
                  "stylesheetParams": {},
                  "destination": "serialized" };
  SaxonJS.transform(options, "async")
    .then(output => {
      let sql = [];
      sql.push("START TRANSACTION;");
      sql.push("UPDATE ARTICLES");
      sql.push(`SET searchtext = '${output.principalResult.replace(/'/g, "''")}'`);
      sql.push(`WHERE uri = '${props.uri}';`);
      sql.push("COMMIT;");
      sql.push(`UPDATE ARTICLES d1`);
      sql.push("SET tokens = to_tsvector(d1.searchtext)");
      sql.push(`FROM ARTICLES d2 WHERE d2.uri = '${props.uri}';`);
      pool.query(sql.join(" "), (error, results) => {
        if (error) {
          console.log(sql);
          console.log(error);
          throw error;
        }
        response.status(202).send("Accepted");
      });
    }).catch(error => {
      response.status(400).send(error);
    });
};

module.exports = upload;
