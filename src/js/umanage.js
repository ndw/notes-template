const bcrypt = require('bcryptjs');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const session = require('express-session');
const {pool} = require('./config');
const render = require('./render');

let userpw = {};
let curuser = {};
const sessionStore = new session.MemoryStore;

passport.serializeUser(function(user, cb) {
  cb(null, curuser);
});

passport.deserializeUser(function(id, done){
  done(null, curuser);
});

function getUser(userid, cb) {
  const sql = "select * from users;";
  pool.query(sql, (error, results) => {
    if (error) {
      throw error;
    }

    results.rows.forEach(row => {
      userpw[row.id] = {
        "name": row.name,
        "password": row.password,
      };
    });

    if (userid in userpw) {
      return cb(null, {
        "username": userid,
        validPassword: function (passwd) {
          if (bcrypt.compareSync(passwd, userpw[userid]["password"])) {
            curuser = {
              "id": userid,
              "name": userpw[userid]
            };
            return true;
          } else {
            return false;
          }
        }
      });
    } else {
      return cb(null, null, { "message": "Login failed, please try again." });
    }
  });
}

passport.use('local-login', new LocalStrategy({
  usernameField : 'username',
  passwordField : 'password'
},
  function(username, password, done) {
    getUser(username, function(err, user) {
      if (err)
        return done(err);
      if (!user)
        return done(null, false, {message: 'Unsuccessful login.'});
      if (!user.validPassword(password))
        return done(null, false, {message: 'Unsuccessful login.'});
      return done(null, user);
    });
  }));

var sessionMiddleware = session({
  secret: process.env.SESSION_SECRET,
  store: sessionStore,
  resave: true,
  saveUninitialized: true,
  cookie: {
    "maxAge": 3600000
  }
});

function sessionHandler(req, res, next) {
  if (req.cookies["connect.sid"]) {
    sessionMiddleware(req, res, next);
  } else {
    next();
  }
}

function sessionCreator(req, res, next) {
  sessionMiddleware(req, res, next);
}

function login(request, response) {
  let message = "";
  if (request.session 
      && "flash" in request.session
      && "error" in request.session.flash) {
    message = request.session.flash.error[0];
  }

  render(request, response, {
    "uri": "/login",
    "message": message,
    "redirect": "/"
  });
};

function logout(req, res) {
  req.logout();
  res.cookie("connect.sid", '', { expires: new Date(0), "path": "/" });
  res.redirect("/");
};

function setupApp(app) {
  app.use(sessionHandler);
  app.use(passport.initialize());
  app.use(passport.session());

  const ppopts = {
    "successReturnToOrRedirect": '/',
    "failureRedirect": '/login',
    "failureFlash": true
  };

  app.get('/login', sessionMiddleware, login);
  app.post('/login', passport.authenticate('local-login', ppopts));
  app.get('/logout', logout);
}

module.exports = {
  "setupApp": setupApp,
};
