"use strict";

const SaxonJS = require('saxon-js');
const {pool} = require('./config');
const render = require('./render');

const homepage = (request, response) => {
  const props = {
    "uri": "/homepage"
  };

  let sql = "SELECT count(uri) FROM articles WHERE uri LIKE '/note/%';";
  pool.query(sql, (error, results) => {
    if (error) {
      console.log(sql);
      console.log(error);
      throw error;
    }

    props.count = results.rows[0].count;
    sql = "SELECT uri,title,pubdate,updated,keywords FROM articles ";
    sql += "WHERE uri LIKE '/note/%' ";
    sql += "ORDER BY updated DESC;";

    pool.query(sql, (error, results) => {
      if (error) {
        console.log(sql);
        console.log(error);
        throw error;
      }

      props.notes = results.rows;

      render(request, response, props);
    });
  });
};

module.exports = homepage;
