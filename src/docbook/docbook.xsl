<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns="http://www.w3.org/1999/xhtml"
                exclude-result-prefixes="xs"
                version="3.0">

<xsl:import href="@@DOCBOOK@@"/>

<xsl:param name="resource-base-uri" select="'/'"/>

</xsl:stylesheet>
