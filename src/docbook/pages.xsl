<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:m="http://docbook.org/ns/docbook/modes"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns="http://www.w3.org/1999/xhtml"
                exclude-result-prefixes="m xs"
                version="3.0">

<xsl:import href="docbook.xsl"/>

<xsl:template match="processing-instruction()" mode="m:docbook">
  <xsl:if test="starts-with(local-name(.), 'x-')">
    <xsl:element name="{string(local-name(.))}"
                 namespace="http://www.w3.org/1999/xhtml"/>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
