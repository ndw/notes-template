#!/usr/bin/env python3

import re
import sys
from urllib.parse import urlparse
import urllib3
import requests
from requests.exceptions import SSLError
import click


class PutArticle:
    def __init__(self, credentials, uri):
        try:
            pos = credentials.index(":")
            self.username = credentials[:pos]
            self.password = credentials[(pos+1):]
        except ValueError:
            raise RuntimeError("Credentials must be username:password: "
                               + credentials)

        if uri.endswith("/"):
            self.uri = uri
        else:
            self.uri = f"{uri}/"

        self.session = requests.Session()
        self.perform_login()

    def put(self, file, note):
        headers = {"content-type": "application/xml"}

        if note is None:
            # This is very crude and will only work sometimes
            if file[0] == "/":
                note = f"note{file}"
            else:
                note = f"note/{file}"
            try:
                pos = note.rindex(".")
                note = note[0:pos]
            except ValueError:
                pass

        if not note.startswith("note/"):
            raise RuntimeError("Note URIs must begin 'note/'")

        try:
            with open(file, 'rb') as dataf:
                print(f"Upload to: {self.uri}{note}")
                resp = self.session.put(f"{self.uri}{note}",
                                        headers=headers, data=dataf)
        except SSLError:
            raise RuntimeError("SSL error: " + self.uri)

        return resp

    def perform_login(self):
        parsed = urlparse(self.uri)
        login = parsed.scheme + "://" + parsed.netloc + "/login"

        resp = self.session.get(login)
        if resp.status_code != 200:
            raise RuntimeError("Failed to get login page: " + login)

        ifield = re.compile("^(.*?)(<input.*?>)(.*)$")
        content = resp.text.replace("\n", " ")
        userfield = None
        passfield = None
        match = ifield.match(content)
        while (not userfield or not passfield) and match:
            input = match.group(2)
            content = match.group(3)
            if re.match(".* type=.text", input) or not(re.match(".* type=", input)):
                userfield = re.sub(".*name=['\"](.*?)['\"].*", r"\1", input)
            if re.match(".* type=.password", input):
                passfield = re.sub(".*name=['\"](.*?)['\"].*", r"\1", input)
            match = ifield.match(content)

        if not userfield or not passfield:
            raise RuntimeError("Failed to parse login page: " + login)

        data = {}
        data[userfield] = self.username
        data[passfield] = self.password

        resp = self.session.post(login, data=data, allow_redirects=False)
        if resp.status_code != 302:
            raise RuntimeError(f"Login failed ({resp.status_code}): {resp.text}")


@click.command()
@click.option("--uri", help="Template server URI", default="http://localhost:8181/")
@click.option("--credentials", "-u", help="Login credentials", required="True")
@click.option("--note", "-n", help="Note URI")
@click.argument("file", type=click.Path(exists=True), required=True)
def putnote(credentials, file, note, uri):
    urllib3.disable_warnings()
    putart = PutArticle(credentials, uri)

    try:
        resp = putart.put(file, note)
    except RuntimeError as err:
        print(err)
        sys.exit(1)

    if resp.status_code != 202:
        print(f"Failed to upload ({resp.status_code}): {uri}")
        sys.exit(1)



if __name__ == "__main__":
    putnote()
