<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:h="http://www.w3.org/1999/xhtml"
                xmlns:f="https://nwalsh.com/ns/functions"
                version="3.0">

<xsl:output method="text" encoding="utf-8"/>

<xsl:param name="srcdir" select="'../build/pages/'"/>

<xsl:template name="xsl:initial-template">
  <xsl:variable name="pages" as="element()+"
                select="collection($srcdir || '?recurse=yes;select=*.html')/*"/>

  <xsl:text>TRUNCATE articles;&#10;</xsl:text>
  <xsl:text>INSERT INTO articles</xsl:text>
  <xsl:text>(uri,authorid,pubdate,updated,title,xmltext,searchtext)</xsl:text>
  <xsl:text> VALUES&#10;</xsl:text>
  <xsl:for-each select="$pages">
    <xsl:apply-templates select="."/>
    <xsl:sequence select="if (position() = last())
                          then ';&#10;'
                          else ',&#10;'"/>
  </xsl:for-each>
  <xsl:text>UPDATE articles d1&#10;</xsl:text>
  <xsl:text>SET TOKENS = to_tsvector(d1.searchtext)&#10;</xsl:text>
  <xsl:text>FROM articles d2;&#10;</xsl:text>
</xsl:template>

<!-- ============================================================ -->

<xsl:template match="h:html" expand-text="yes">
  <xsl:variable name="uri"
                select="substring-after(base-uri(.), '/build/pages')
                        => substring-before('.html')"/>

  <xsl:text>(</xsl:text>
  <xsl:text>{f:string($uri)},'admin',</xsl:text>
  <xsl:text>{f:string(string(current-dateTime()))},</xsl:text>
  <xsl:text>{f:string(string(current-dateTime()))},</xsl:text>
  <xsl:text>{f:string(h:head/h:title)},</xsl:text>
  <xsl:text>XMLPARSE (DOCUMENT '</xsl:text>
  <xsl:sequence select="replace(serialize(.), '''', '''''')"/>
  <xsl:text>'),</xsl:text>

  <xsl:variable name="text" as="xs:string*">
    <xsl:for-each select=".//text()">
      <xsl:if test="normalize-space(.) != ''">
        <xsl:sequence select="normalize-space(.)"/>
      </xsl:if>
    </xsl:for-each>
  </xsl:variable>

  <xsl:text>{f:string(string-join($text, ' '))}</xsl:text>
  <xsl:text>)</xsl:text>
</xsl:template>

<!-- ============================================================ -->

<xsl:function name="f:string">
  <xsl:param name="str"/>
  <xsl:sequence select="'''' || replace(string($str), '''', '''''') || ''''"/>
</xsl:function>

</xsl:stylesheet>
